Design Science Timeline
=======================

####[Running Demonstration Here](http://ode.engin.umich.edu/timeline)

The purpose of the Design Science History Timeline is to relate historical events with design-related metrics such as publication counts for various design methodologies.
The historical events span 6 distinct categories - social, political, economic, academic, major design trends, and notable products.

The historical events are captured in an interactive, scrollable timeline, built off the great VeriteJS timeline codebase.
The plotting functionality was implemented with D3.js, as well as NVD3.js elements.  Connecting these two major pieces of the codebase uses a variety of jQuery code.

Please feel free to clone/fork this code, or contact for any assistance.
